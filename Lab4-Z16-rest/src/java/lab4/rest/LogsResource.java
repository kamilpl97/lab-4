/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.rest;

import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lab4.Log;
import lab4.LogBean;

/**
 * REST Web Service
 *
 * @author Windows
 */
@Path("/logs")
public class LogsResource {

    @Context
    private UriInfo context;
    
    @EJB
    LogBean bean;

    /**
     * Creates a new instance of LogssResource
     */
    public LogsResource() {
    }

    /**
     * Retrieves representation of an instance of lab4.rest.LogsResource
     * @return an instance of List<Log>
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<Log> getXml() {
        //TODO return proper representation object
        return bean.getLogList();
    }

    /**
     * POST method for creating an instance of LogsResource
     * @param content representation for the new resource
     * @return an HTTP response with content of the created resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response postXml(Log content) {
        //TODO
        bean.addLog(content);
        return Response.created(context.getAbsolutePath()).build();
    }

}
