/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import javax.ejb.Singleton;

/**
 *
 * @author Windows
 */
@Singleton
public class LogBean {

    List<Log> logList = new ArrayList();
    Integer id = 0;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public List<Log> getLogList() {
        return Arrays.asList(logList.toArray(new Log[logList.size()]));
    }

    public void addLog(Log log) {
        log.setId(++id);
        logList.add(log);
    }
}
