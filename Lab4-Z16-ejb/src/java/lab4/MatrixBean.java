/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import matrix.Matrix;
import matrix.PrimitiveMatrix;
import matrix.determinant.DeterminantProcessorSarus;

/**
 *
 * @author Windows
 */
@Stateless
public class MatrixBean implements MatrixBeanLocal {

    private boolean failed = false;
    public Matrix matrix;
    private final LogsRestClient logsClient = new LogsRestClient();

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public boolean input(String inputString) {
        failed = false;
        if (inputString.isEmpty()) {
            return false;
        }
        String[] rows = inputString.split("; ");
        int size = rows.length;
        if (size != 2 && size != 3) {
            return false;
        }
        String[][] cells = new String[size][];
        double[][] data = new double[size][size];
        int i = 0;
        for (String row : rows) {
            String[] rowCells = row.split(" ");
            int rowSize = rowCells.length;
            if (rowSize != size) {
                failed = true;
                return false;
            }
            cells[i] = rowCells;
            i++;
        }
        try {
            for (i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    data[i][j] = Double.parseDouble(cells[j][i]);
                }
            }
        } catch (NumberFormatException ex) {
            failed = true;
        }
        matrix = new PrimitiveMatrix(data);
        return true;
    }

    @Override
    public double determinant() {
        double result = DeterminantProcessorSarus.INSTANCE.getDeterminant(matrix);
        logsClient.postXml(new Log(matrix.toString(), result));
        return result;
    }

    @Override
    public List<Log> getLogs() {
        return logsClient.getXml(LogList.class).getLogs();
    }

    static class LogsRestClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:8080/Lab4-Z16-rest/rest";

        public LogsRestClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            webTarget = client.target(BASE_URI).path("logs");
        }

        public <T> T getXml(Class<T> responseType) throws ClientErrorException {
            WebTarget resource = webTarget;
            return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
        }

        public Response postXml(Object requestEntity) throws ClientErrorException {
            return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_XML).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML), Response.class);
        }

        public void close() {
            client.close();
        }
    }

}
