/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Windows
 */
@Local
public interface MatrixBeanLocal {

    public boolean input(String input);

    public double determinant();

    public List<Log> getLogs();
    
}
