/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Windows
 */
@XmlRootElement(name = "logs")
public class LogList {
    
    private List<Log> logs;

    public LogList() {
    }

    public LogList(List<Log> logs) {
        this.logs = logs;
    }

    @XmlElement(name = "log")
    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }
    
    
}
