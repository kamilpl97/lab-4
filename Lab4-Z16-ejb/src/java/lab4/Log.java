/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Windows
 */
@XmlRootElement(name = "log")
public class Log {

    private int id;
    private String matrix;
    private double determinant;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Log() {
    }

    public Log(String matrix, double determinant) {
        this.matrix = matrix;
        this.determinant = determinant;
    }

    public String getMatrix() {
        return matrix;
    }

    public void setMatrix(String matrix) {
        this.matrix = matrix;
    }

    public double getDeterminant() {
        return determinant;
    }

    public void setDeterminant(double determinant) {
        this.determinant = determinant;
    }

}
