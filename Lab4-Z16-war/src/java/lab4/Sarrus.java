/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import matrix.MatrixException;

/**
 *
 * @author Windows
 */
@WebServlet(name = "Sarrus", urlPatterns = {"/Sarrus"})
public class Sarrus extends HttpServlet {

    @EJB(name = "MatrixBean")
    private MatrixBeanLocal Bean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String act = request.getParameter("act");

        String numbers = request.getParameter("numbers");
//        int amount = Integer.parseInt(request.getParameter("amount"));

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<head><meta><link rel='stylesheet' href='Style/css/components.css'>");
            out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
            out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
            out.println("<title>Wynik</title>");
            out.println("</head>");
            out.println("<body>");
            if (act == null) {
                //no button has been selected
            } else if (act.equals("input")) {
                //delete button was pressed
            }
            else if (act.equals("log")) {

                out.println("<h1>Historia</h1><br/>");
                for (Log log : Bean.getLogs()) {

                    out.println(log.getId() + ". " + log.getMatrix() + " wynik: " + Double.toString(log.getDeterminant()));
                    out.println("<br/>");
                }

                out.println("");
                out.println("</body>");
                out.println("</html>");

                out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
                out.println(request.getHeader("referer"));
                out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
                return;
            } 
            
            else {
                //someone has altered the HTML and sent a different value!
            }

            if (Bean.input(numbers)) {
                try{
                out.println("<h1>Wyznacznik</h1><h2>macierzy: " + numbers + "<br/> to " + Bean.determinant() + "<br/>");
                }
                catch (MatrixException e){
                    out.println("<h2>Błędne dane wejściowe (zły wymiar macierzy)</h2>");
                }
            } else {
                out.println("<h2>Błędne dane wejściowe</h2>");
            }

            out.println("");
            out.println("</body>");
            out.println("</html>");

            out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
            out.println(request.getHeader("referer"));
            out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
